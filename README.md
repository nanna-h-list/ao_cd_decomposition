## Introduction
This program performs an atomic-orbital decomposition of the circular dichroic signal into fragment
contributions based on selections of atoms.

## Description
For help, run      python ecd_decomposition.py -h

Normal run: 

    python ecd_decomposition.py <Gaussian output file> <MO Coefficient file> --out <output filename>

    if no '--out' is given, the output will be saved to 'CD_MO_output.txt'

    Note: During execution, a number of .npy files will be saved for faster rerun.
    use the   
        --reload        flag to avoid reading in the Gaussian output file again.

    if the run stops before the end, resume the calculation by adding the 
        --resume        flag to the previous execution command


## References
Author:     Nanna Holmgaard List

If you use this, please cite:

- Origin of DNA-Induced Circular Dichroism in a Minor-Groove Binder. 
  Nanna Holmgaard List, Jeré mie Knoops, Jenifer Rubio-Magnieto, Julien Ide,
  David Beljonne, Patrick Norman, Mathieu Surin, and Mathieu Linares
  J. Am. Chem. Soc., 2017, 139 (42), pp 14947–14953

